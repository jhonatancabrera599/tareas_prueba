import React, {useState, useEffect} from 'react';
import Herader from './components/Header';
import ListaList from './components/ListaList';
import Form from './components/Form';

import './App.css';
function App() {

  const initialState = JSON.parse(localStorage.getItem("lista")) || [];
  const [input, setInput] = useState("");
  const [lista, setLista] = useState(initialState);
  const [editLista, setEditLista] = useState(null);

  useEffect(() => {
    localStorage.setItem("lista", JSON.stringify(lista));
  }, [lista]);

  return (
    <div className='container'>
      <div className='app-wrapper'>
        <div>
          <Herader />
        </div>
        <div>
          <Form
            input={input}
            setInput={setInput}
            lista={lista}
            setLista={setLista}
            editLista={editLista}
            setEditLista={setEditLista} />
        </div>
        <div>
          <ListaList
            lista={lista}
            setLista={setLista}
            setEditLista={setEditLista} />
        </div>
      </div>
    </div>
  );
}

export default App;

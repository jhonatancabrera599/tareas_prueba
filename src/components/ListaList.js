import React from "react";

const ListaList = ({lista, setLista, setEditLista}) => {

    function handleComplete(lista) {
        setLista(
            lista.map((item) => {
                if (item.id === lista.id) {
                    return { ...item, completed: !item.completed };
                }
                return item;
            })
        );
    }

    function handleEdit({ id }) {
        const findLIst = lista.find((lista) => lista.id === id);
        setEditLista(findLIst);
    }

    function handleDelete({ id }) {
        setLista(lista.filter((lista) => lista.id !== id));
    }

    return ( 
     <div>
        {lista.map((lista) =>(
            <li className="list-item" key={lista.id}>
                <input 
                type="text"
                value = {lista.title}
                className = {`list ${lista.completed ? "complete" : ""}`}
                onChange={(event) => event.preventDefault()}
                />
                <div>
                    <button className="button-complete task-button" 
                        onClick={() => handleComplete(lista)}>
                        <i className="fa fa-check-circle"></i>
                    </button>

                    <button className="button-edit task-button" 
                    onClick={() => handleEdit(lista)}>
                        <i className="fa fa-edit"></i>
                    </button>

                    <button className="button-delete task-button" 
                        onClick={() => handleDelete(lista)}>
                        <i className="fa fa-trash"></i>
                    </button>
                </div>
            </li>
        ))}    
    </div>
   );
};

export default ListaList;
import React, {useEffect} from "react";
import {v4 as uuidV4 } from "uuid";

function Form({ input, setInput, lista, setLista, editLista, setEditLista }) {

    const updateLista = (title, id, completed) => {
        const newLista = lista.map((lista) => lista.id === id ? { title, id, completed } : lista
        );
        setLista(newLista);
        setEditLista("");
    };

    useEffect(() => {
        if (editLista) {
            setInput(editLista.title);
        } else {
            setInput("");
        }
    }, [setInput, editLista]);

    function onInputChange(event) {
        setInput(event.target.value);
    }

    function onFormSubmit(event) {
        event.preventDefault();
        if (!editLista) {
            setLista([...lista, { id: uuidV4(), title: input, completed: false }]);
            setInput("");
        } else {
            updateLista(input, editLista.id, editLista.completed);
        }
    }

    return (
        <form onSubmit={onFormSubmit}>
            <input
                type="text"
                placeholder="Escribir tarea"
                className="task-input"
                value={input}
                required
                onChange={onInputChange} />
            <button className="button-add" type="submit">
                {editLista ? "OK" : "Agregar"}
            </button>
        </form>
    );
}

export default Form;